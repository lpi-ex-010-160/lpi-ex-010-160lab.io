#!/bin/bash -x

cd $(dirname $0)

### copy everything to a tmp dir
rm -rf tmp/
rsync -a . tmp/
cd tmp/

### preprocess markdown files to fix them for pandoc
for file in $(find . -name '*.md'); do
    # fix latex delimiters for pandoc
    sed -i $file \
        -e 's/$`/$/g' \
        -e 's/`\$/$/g'

    # leave an empty line before starting a list, otherwise pandoc does not interpret it as a list
    sed -i $file \
        -e '/./{H;$!d} ; x ; s/:\n-/:\n\n-/'

    # open links on a new tab
    sed -i $file \
        -e 's#\([^(]\)\(https\?://.*\)#\1[\2](\2){target="_blank"}#'
done

### render all the formats
rm -rf ../public/
Rscript -e "bookdown::render_book('index.Rmd', 'all', '.pdf', output_dir = '../public')"

### copy font Monaco
cd ..
cp assets/Monaco.woff ../public/

### clean up
rm -rf tmp/
