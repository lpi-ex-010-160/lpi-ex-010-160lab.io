
# Computers, Software and Operating Systems {#index}

## Learning Goals 

- Learn the fundamentals of computer hardware.

- Have knowledge of various operating systems and what they have in common, as well as the ability to evaluate differences

## Previous Knowledge 

- Basic computer skills are useful

## Introduction 

The physical components of a computer, such as the case, central processing unit (CPU), monitor, mouse, keyboard, computer data storage, graphics card, sound card, speakers, and motherboard, are referred to as computer hardware.

Software, on the other hand, is a collection of instructions that can be stored and executed by hardware. Hardware is so-called because it is _"hard"_ or rigid in terms of change, whereas software is _"soft"_ because it is easily changed.

The software typically directs hardware to carry out any command or instruction. A usable computing system is made up of a combination of hardware and software, though other systems exist that only use hardware.

Linux is essentially a piece of software in the form of an operating system; one of Linux's responsibilities is to provide software with interfaces to access the hardware of a system. The majority of hardware configuration details are beyond the scope of this lesson. However, users are frequently concerned about the performance, capacity, and other aspects of system hardware because they affect a system's ability to adequately support specific applications. This lesson looks at hardware as distinct physical items that use standard connectors and interfaces. The standards are fairly constant. However, the form factor, performance, and capacity characteristics of hardware are always changing. Regardless of how changes may blur physical distinctions, the concepts of hardware described in this lesson remain applicable.


## Version

The latest version of this book is at:
http://lpi-ex-010-160.thelinuxtrainer.com/

Corrections or improvements can be suggested at:
https://gitlab.com/dashohoxha/bookdown-example

## License

![](assets/images/cc.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 Unported License](http://creativecommons.org/licenses/by-sa/4.0/).
